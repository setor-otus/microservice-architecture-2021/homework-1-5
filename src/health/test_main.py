from fastapi.testclient import TestClient

from health.main import app

client = TestClient(app)


def test_read_health():
    response = client.get('/health/')
    assert response.status_code == 200
    assert response.json() == {'status': 'OK'}


def test_read_readiness_probe():
    response = client.get('/health/readiness')
    assert response.status_code == 200
    assert response.text == 'OK'


def test_read_liveness_probe():
    response = client.get('/health/liveness')
    assert response.status_code == 200
    assert response.text == 'OK'
