from fastapi import FastAPI
from fastapi.responses import PlainTextResponse

app = FastAPI(
    docs_url='/',
)


@app.get('/health/')
async def health():
    return {'status': 'OK'}


@app.get('/health/readiness', response_class=PlainTextResponse)
async def health_readiness():
    return 'OK'


@app.get('/health/liveness', response_class=PlainTextResponse)
async def health_liveness():
    return 'OK'
